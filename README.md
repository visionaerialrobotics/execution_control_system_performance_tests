# Execution and performance tests
In this repository you can find the files and results obtained after the execution of performance test to which the executive system has been subjected. The structure of the repository is the following one:

```
CPU & memory usage/                                    -- Results of CPU & memory usage tests
      performance_summary.csv                          -- Joint summary of the results of both interpreters
      behavior_tree_interpreter/                       -- Results for behavior tree interpreter
	      behavior_tree_interpreter_performance.csv    -- Summary of performance results for behavior tree interpreter
	      system monitor/                              -- Performance results obtained from Linux System Monitor
		      bash.jpg                                 -- Bash performance
		      nodelet.jpg                              -- Nodelets performance
		      roslaunch.jpg                            -- Roslaunchs performance
		  top/                                         -- Performance results obtained from top command
		      top.jpg                                  -- Top command output 
       python interpreter/                             -- Results for behavior tree interpreter
	      python_interpreter_performance.csv           -- Summary of performance results for python interpreter
	      system monitor/                              -- Performance results obtained from Linux System Monitor
		      bash.jpg                                 -- Bash performance
		      nodelet.jpg                              -- Nodelets performance
		      roslaunch.jpg                            -- Roslaunchs performance
		  top/                                         -- Performance results obtained from top command
		      top.jpg                                  -- Top command output 
execution time/                                        -- Results of execution time tests
      behavior tree interpreter/                       -- Results for behavior tree interpreter
	      behavior_tree_interpreter_times.csv          -- Summary of execution time results for behavior tree interpreter
	      behavior_tree_interpreter_times.txt          -- Raw file with times obtained from tests
	 python interpreter/                               -- Results for python interpreter
	      python_interpreter_times.csv                 -- Summary of execution time results for python interpreter
	      python_interpreter_times.txt                 -- Raw file with times obtained from tests
	    
```
The format of the time samples in the  .txt time files is the next one:

 - **behavior**: managed behavior in the moment when the sample is taken.
 - **time**: ROS time of the sample in seconds and nanoseconds.
 - **label**: component in which the sample is taken; description of the moment in which the sample is taken.

The times indicated in the .csv time files were obtained in the following way:

 - **Mission control time**: sample with label: "Behavior Coordinator; Received message to activate: `X`" sample with label: "Behavior Tree Interpreter; Behavior activation finished `X-1`"
 - **Coordination time**:  sample with label: "Behavior Coordinator;  List of behaviors to activate and deactivate for behavior:  `X`" - sample with label: "Behavior Coordinator; Received message to activate: `X`"
 - **Activation time**:  sample with label:"Behavior Coordinator;  Deactivation time for behavior: `X`" - sample with label: "Behavior Coordinator;  List of behaviors to activate and deactivate for behavior:  `X`"

